FROM registry.gitlab.com/vhuezo/oracle-jdk/centos8/oracle-jdk:8u211
#FROM registry.gitlab.com/vhuezo/oracle-jdk:latest
#FROM registry.gitlab.com/vhuezo/oracle-jdk/centos8:8u211


MAINTAINER Victor Huezo <huezohuezo.1990@gmail.com>
LABEL maintainer="Victor Huezo <huezohuezo.1990@gmail.com>"


ARG JAR

ENV JAR_FILE=$JAR

ENV directorio /directorio
ENV PORT 8038
WORKDIR ${directorio}   
#ADD ./build/libs/*.jar $directorio/$JAR_FILE

COPY ./build/libs/*.jar $directorio/$JAR_FILE

ENV TZ=America/El_Salvador
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

HEALTHCHECK --interval=1m CMD  curl --fail --silent  localhost:8038/ggsn/env || exit 1


EXPOSE 8038

#Comando para ejecutar JAVA en Docker
CMD java -Dserver.port=$PORT   -Duser.timezone=America/El_Salvador -jar  $directorio/$JAR_FILE
 

